<?php

namespace App\Http\Controllers\Api;

use App\Events\TeamJoined;
use App\Team;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\User;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Validator;

class AuthController extends Controller
{
    public function register (Request $request) {

        $validator = Validator::make($request->all(), [
//            'name' => 'required|string|max:255',
        ]);

        if ($validator->fails())
        {
            return response(['errors'=>$validator->errors()->all()], 422);
        }

        $request['password'] = 0;
        $request['email'] = 0;
        $request['isPlayer'] = 1;
        $user = User::create($request->toArray());

        $token = $user->createToken('Laravel Password Grant Client')->accessToken;
        $response = ['token' => $token];

        return response($response, 200);

    }

    public function login (Request $request) {

        $team = Team::where('name', $request->name)
            ->where('isPlayer', 1)
            ->first();


//        $user = User::where('email', $request->email)->first();
//
//        if ($user) {
//
//            if (Hash::check($request->password, $user->password)) {
//                $token = $user->createToken('Laravel Password Grant Client')->accessToken;
//                $response = ['token' => $token];
//                return response($response, 200);
//            } else {
//                $response = "Password missmatch";
//                return response($response, 422);
//            }
        if ($team) {
            $token = $team->createToken('Laravel Password Grant Client')->accessToken;
            $response = ['token' => $token];
            // send to pusher
            event(new TeamJoined($team->name));
            return response($response, 200);
        } else {
            $response = 'User does not exist';
            return response($response, 422);
        }

    }

    public function logout (Request $request)
    {

        $token = $request->user()->token();
        $token->revoke();

        $response = 'You have been succesfully logged out!';
        return response($response, 200);
    }
}
